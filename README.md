Considerando que o avaliador já tenha um servidor localhost configurado para rodar WordPress, este projeto foi exportado utilizando o plugin Duplicador para facilitar e economizar trabalho com a primeira instalação.

## **git clone https://KenedyMorais@bitbucket.org/KenedyMorais/test-brandcenter.git**

## Start Project

1. Faça um git clone do repositório citado acima na pasta raiz do localhost.
2. Acesse http://localhost/test-brandcenter/installer.php
3. Na primeira etapa, aceite os termos de uso e continue.
4. Na segunda etapa, na opção **Action**, selecione a opção _Create New Database_.
5. Crie um nome para o banco de dados e informe o user e pass de acesso ao banco (como este é para fins de teste, pode-se utilizar o usuário root do banco de dados).
6. Clique em **Test Database**, uma vez que o alerta apresentar o status Good na Validation, clique em **Next**.
7. Na terceira etapa, confira o campo URL e certifique-se de que está apontando para o diretório de instalação do WordPress e clique em **Next**.
8. Na quarta etapa, deixe marcada a opção _Auto delete installer files after login_ e clique em **Admin Login**
9. Insira **admin** no login e senha para realizar o login.

---

## Informações essenciais

Para este teste foi utilizado o theme hello-ellementor. Para criar o modal na página Home foi utilizada a biblioteca bootstrap que pode ser encontrada em TEMPLATE DIRECTORY/includes/popup/

A função javascript para o filtro da página de arquivos pode ser encontrado no arquivo TEMPLATE DIRECTORY/main.js

O carrossel da home foi criado utilizando o widget nativo do Elementor e configurado com javascript encontrado em TEMPLATE DIRECTORY/main.js

No arquivo functions.php, o código para ser avaliado encontra-se a partir da linha 190.

Na função archivesLoop() que encontra-se em functions.php, altere o valor da variável $requiredLogin para _true_ caso o download de arquivos na página arquivos exija usuário logado.

A query para retornar os arquivos de imagem utilizada na página de arquivos encontra-se em TEMPLATE DIRECTORY/includes/files-list.php

---

## Considerações

Para fins de teste, não me aprofundei muito em questões de layout já que não tinha um exemplo real de empresa para criar um website completo.

O melhor caminho para atender as necessidades da brandcenter pensando em um produto escalável, talvez seja começar com a criação de um plugin de Addons para Elementor próprio da brandcenter.

Desculpem-me não ter entregado antes, como estou de saida de uma função política, precisei dividir meu tempo entre o teste e a resolução de algumas situações no meu antigo emprego.
